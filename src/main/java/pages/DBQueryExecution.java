package pages;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;



public class DBQueryExecution {
	
	public static ResultSet executeQuery(String  query, Connection connection,int number) throws SQLException {

		
		PreparedStatement preparedStatement = connection.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE , 
		         ResultSet.CONCUR_UPDATABLE);
		preparedStatement.setInt(1,number);  
		

        ResultSet resultSet = preparedStatement.executeQuery();
		
		return resultSet;
	}

public static int findNumberOfRows(ResultSet resultSet) throws SQLException {
	int counter =0;	
	while (resultSet.next()) {
    	counter++;
 
        System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"); // Move to the next line for the next row
    }
		System.out.println("counter is + "+counter);
		return counter;
	}

public static ArrayList<String> listOfColumns(ResultSet resultSet) throws SQLException {
	ArrayList<String> actualCols = new ArrayList<String>();
	ResultSetMetaData metaData = resultSet.getMetaData();
    int columnCount = metaData.getColumnCount();

    for (int i = 1; i <= columnCount; i++) {
    	actualCols.add(metaData.getColumnName(i));
       
    }
    System.out.println("actual list of cols are : "+actualCols);
	
	
	return actualCols;
}
public static String fetchDataByColumnName(ResultSet resultSet,String ColumnName) throws SQLException {
	
	
    resultSet.first();
   
        
    return resultSet.getDate(ColumnName).toString();
}

}
